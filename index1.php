<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JSON USING SQL</title>
    <link href="./style.css" rel="stylesheet">
    <script>
    function getDetails() {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "http://localhost/json/getDetails.php", false);
        xmlhttp.send()
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var result = xmlhttp.responseText;
            // alert(result)
            var o = JSON.parse(result)
            var data = "";
            for (i = 0; i < o.length; i++) {
                data += '<table>\
                <tr><th>Model</th><td>' + o[i].model + '</td><td rowspan=8><img src=' + o[i].image + '></td></tr>\
                <tr><th>brand</th><td>' + o[i].brand + '</td></tr>\
                <tr><th>mrp</th><td>' + o[i].mrp + '</td></tr>\
                <tr><th>price</th><td>' + o[i].price + '</td></tr>\
                <tr><th>ram</th><td>' + o[i].ram + '</td></tr>\
                <tr><th>storage</th><td>' + o[i].storage + '</td></tr>\
                <tr><th>camera</th><td>' + o[i].camera + '</td></tr>\
                <tr><th>isAvailable</th><td>' + o[i].isAvailable + '</td></tr>\
                </table>';
            }
            document.getElementById("show").innerHTML = data;
        }
    }
    </script>
</head>

<body onload="getDetails()">
    <h1>Mobile Gallery By Mysql table</h1>
    <div id="show" class="container"></div>
</body>

</html>