//javascript object
var user = new Object()
user.name = "Srini"
user.age = 22
// document.write("my name is " + user.name + "my age is " + user.age + "<br>")

//javascript literal
var user1 = {
    name: "Vicky",
    age: 21,
    fullDetails: function () {
        return this.name + this.age
    }
}
// document.write("my name is " + user1.name + "my age is " + user1.age + user1.fullDetails() + "<br>")

//json
var user2 = {
    "name": "Vicky",
    "age": 21,
}
// document.write("my name is " + user2.name + "my age is " + user2.age + "<br>")
// alert(typeof (user2))
var user2ex = JSON.stringify(user2)
// alert(typeof (user2)) //object
// alert(typeof (user2ex)) //string

//javscript define json format
var user3 = '{"name":"ram","age":"24"}';
// alert(typeof (user3)) //string
var user3convertJson = JSON.parse(user3)
// alert(typeof (user3)) //object
// document.write(user3convertJson.name)